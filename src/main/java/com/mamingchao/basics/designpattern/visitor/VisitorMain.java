package com.mamingchao.basics.designpattern.visitor;

/**
 * Created by mamingchao on 2020/10/20.
 */
public class VisitorMain {

    public static void main(String[] args) {
        Employee student = new Employee();

        new Computor(student);
        System.out.println(student.totalPrice);
    }

}

class Computor{
    computerPart cpu = new CPU();
    computerPart memory = new Memory();
    computerPart board = new Board();

    Computor(Visitor_ v){
        cpu.accept(v);
        memory.accept(v);
        board.accept(v);
    }

}


abstract class computerPart{
    abstract void accept(Visitor_ visitor_);
}

class CPU extends computerPart{

    int price = 100;

    @Override
    void accept(Visitor_ visitor_) {
        visitor_.visitCpuPrice(this);

    }

}

class Memory extends computerPart{
    int price = 200;
    @Override
    void accept(Visitor_ visitor_) {

        visitor_.visitMemoryPrice(this);
    }

}

class Board extends computerPart{
    int price = 300;

    @Override
    void accept(Visitor_ visitor_) {
        visitor_.visitBoardPrice(this);
    }
}

interface Visitor_{
    void visitCpuPrice(CPU cpu);
    void visitMemoryPrice(Memory memory);
    void visitBoardPrice(Board board);
}


class Student implements Visitor_ {

    double totalPrice;

    @Override
    public void visitCpuPrice(CPU cpu) {
        totalPrice = totalPrice + cpu.price * 0.6;
    }

    @Override
    public void visitMemoryPrice(Memory memory) {
        totalPrice = totalPrice + memory.price * 0.6;
    }

    @Override
    public void visitBoardPrice(Board board) {
        totalPrice = totalPrice + board.price * 0.6;
    }
}


class Employee implements Visitor_ {

    double totalPrice;

    @Override
    public void visitCpuPrice(CPU cpu) {
        totalPrice = totalPrice + cpu.price * 1.5;
    }

    @Override
    public void visitMemoryPrice(Memory memory) {
        totalPrice = totalPrice + memory.price * 1.5;
    }

    @Override
    public void visitBoardPrice(Board board) {
        totalPrice = totalPrice + board.price * 1.5;
    }
}
