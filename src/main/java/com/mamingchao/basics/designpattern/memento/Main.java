package com.mamingchao.basics.designpattern.memento;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 这里是模拟 游戏里的游戏存档
 *
 *
 * Created by mamingchao on 2020/10/21.
 */
public class Main {

    public static void main(String[] args) {
        save();

        load();
    }

    static void load(){
        GameObject go = null;
        Bullet bullet = null;
        ObjectInput objectInput = null;
        try {
            File f = new File("/Users/mamingchao/work/viewhigh/designmodel/src/images/object.data");
            objectInput = new ObjectInputStream(new FileInputStream(f));
            go = (GameObject)objectInput.readObject();
            bullet = (Bullet)objectInput.readObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                objectInput.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println(go.toString());
        System.out.println("----------------------------------------");
        System.out.println(bullet.toString());
    }

    static void save(){
        GameObject go = getGameObject();
        Bullet bullet = new Bullet(100,"red");
        ObjectOutput objectOutput = null;
        try {
            File f = new File("/Users/mamingchao/work/viewhigh/designmodel/src/images/object.data");
            objectOutput = new ObjectOutputStream(new FileOutputStream(f));
            objectOutput.writeObject(go);
            objectOutput.writeObject(bullet);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                objectOutput.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    static GameObject getGameObject(){
        Tank tank = new Tank.TankBuider()
                .buildBasicInfo("神棍德","yellow")
                .buildPosition(10,10,20,90)
                .build();

        List<Bullet> bullets = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Bullet bullet = new Bullet(i,"blue");
            bullets.add(bullet);
        }

        return new GameObject(tank,bullets);
    }
}



class GameObject implements Serializable{
    Tank tank;
    List<Bullet> bulletList;

    public GameObject(Tank tank, List<Bullet> bulletList) {
        this.tank = tank;
        this.bulletList = bulletList;
    }

    @Override
    public String toString() {
        return "GameObject{" +
                "tank=" + tank +
                ", bulletList=" + bulletList +
                '}';
    }
}

class Tank implements Serializable{
    String name;
    String color;
    int x,y,w,h;

    static class TankBuider{
        Tank tank = new Tank();
        TankBuider buildBasicInfo(String name,String color){
            tank.name = name;
            tank.color = color;
            return this;
        }

        TankBuider buildPosition(int x,int y,int w,int h){
            tank.x = x;
            tank.y = y;
            tank.w = w;
            tank.h = h;
            return this;
        }

        Tank build(){
            return tank;
        }

        @Override
        public String toString() {
            return "TankBuider{" +
                    "tank=" + tank +
                    '}';
        }
    }

}

class Bullet implements Serializable{
    int size;
    String color;

    public Bullet(int size, String color) {
        this.size = size;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Bullet{" +
                "size=" + size +
                ", color='" + color + '\'' +
                '}';
    }
}
