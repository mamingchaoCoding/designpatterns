package com.mamingchao.basics.designpattern.decorator.a_little_try;

import javax.swing.*;
import java.awt.*;

/**
 * 因为这里 人物原型 需要继承JFrame，不能继承多个父类，所以Morrigan 变通的设计成接口
 *
 * Created by mamingchao on 2020/10/13.
 */
public class OriginalMorrigan extends JFrame implements Morrigan{

    private String imageName = "Morrigan0.jpg";

    public void setImageName(String newImageName){
        this.imageName = newImageName;
    }

    @Override
    public void display() {
        this.setLayout(new FlowLayout());
        JLabel l1=new JLabel(new ImageIcon("src/images/" + this.imageName));
        this.add(l1);
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
}
