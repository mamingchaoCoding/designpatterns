package com.mamingchao.basics.designpattern.decorator.democode;

/**
 * Created by mamingchao on 2020/10/12.
 */
public class BloodDecorator extends GoDecorator{

    public BloodDecorator(GameObject o) {
        super(o);
    }


    public void display() {
        o.display();
        System.out.println(o.toString() + "has been added blood ");
    }
}
