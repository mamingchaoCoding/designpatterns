package com.mamingchao.basics.designpattern.decorator.democode;

/**
 * Created by mamingchao on 2020/10/12.
 */
public class Tank extends GameObject{
    /**
     * 所有的游戏对象 都需要外观显示功能
     */
    @Override
    public void display() {
        System.out.println("display Tank outlook");
    }
}
