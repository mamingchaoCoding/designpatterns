package com.mamingchao.basics.designpattern.decorator.democode;

/**
 * 用抽象对象，是因为这是真实存在的物体，是名词，不是形容词（Readable，Closeable）
 *
 * Created by mamingchao on 2020/10/12.
 */
public abstract class GameObject {

    /**
     * 所有的游戏对象 都需要外观显示功能
     */
    public abstract void display();


}
