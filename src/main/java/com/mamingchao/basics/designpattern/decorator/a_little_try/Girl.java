package com.mamingchao.basics.designpattern.decorator.a_little_try;

/**
 * Created by mamingchao on 2020/10/13.
 */
public class Girl extends Changer{

    public Girl(Morrigan m) {
        super(m);
    }

    public void display() {
        ((OriginalMorrigan)super.m).setImageName("Morrigan2.jpg");
        m.display();
    }


}
