package com.mamingchao.basics.designpattern.decorator.a_little_try;

/**
 *
 * 抽象构件角色：莫莉卡
 *
 * 真实构件-人物原型 需要继承JFrame，不能继承多个父类，所以Morrigan 变通的设计成接口
 *
 * Created by mamingchao on 2020/10/13.
 */
public interface Morrigan {

    void display();
}
