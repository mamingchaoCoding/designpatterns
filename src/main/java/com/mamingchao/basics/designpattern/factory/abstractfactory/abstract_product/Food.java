package com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product;

/**
 * Created by mamingchao on 2020/10/12.
 */
public abstract class Food {
    public abstract void eat();
}
