package com.mamingchao.basics.designpattern.factory.factorymethod;

/**
 * Created by mamingchao on 2020/10/12.
 */
public class Car implements Moveable{
    @Override
    public void go() {
        System.out.println(" factory method car go");
    }
}
