package com.mamingchao.basics.designpattern.factory.abstractfactory.specific_factory;

import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_factory.AbstractFactory;
import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Food;
import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Vehicle;
import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Weapon;
import com.mamingchao.basics.designpattern.factory.abstractfactory.specific_product.AK47;
import com.mamingchao.basics.designpattern.factory.abstractfactory.specific_product.Bread;
import com.mamingchao.basics.designpattern.factory.abstractfactory.specific_product.Car;

/**
 * Created by mamingchao on 2020/10/12.
 */
public class ModernFactory extends AbstractFactory{
    @Override
    public Food createFood() {
        return new Bread();
    }

    @Override
    public Vehicle createVehicle() {
        return new Car();
    }

    @Override
    public Weapon createWeapon() {
        return new AK47();
    }
}
