package com.mamingchao.basics.designpattern.factory.factorymethod;

/**
 * Created by mamingchao on 2020/10/12.
 */
public class MagicFactory implements Factory{
    @Override
    public Moveable getMoveable() {
        return new Broom();
    }
}
