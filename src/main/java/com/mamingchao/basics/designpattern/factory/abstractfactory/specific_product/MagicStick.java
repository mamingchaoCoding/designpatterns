package com.mamingchao.basics.designpattern.factory.abstractfactory.specific_product;

import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Weapon;

/**
 * Created by mamingchao on 2020/10/12.
 */
public class MagicStick extends Weapon {
    @Override
    public void shoot() {
        System.out.println("biu biu biu");
    }
}
