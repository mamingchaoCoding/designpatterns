package com.mamingchao.basics.designpattern.factory.abstractfactory.specific_product;

import com.mamingchao.basics.designpattern.factory.abstractfactory.abstract_product.Food;

/**
 * Created by mamingchao on 2020/10/12.
 */
public class Mushroom extends Food{
    @Override
    public void eat() {
        System.out.println("qiu qiu qiu");
    }
}
