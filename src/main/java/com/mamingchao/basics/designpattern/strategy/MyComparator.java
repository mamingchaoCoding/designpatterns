package com.mamingchao.basics.designpattern.strategy;

/**
 * Created by mamingchao on 2020/9/27.
 */

//这个注解打不打都可以
//@FunctionalInterface
public interface MyComparator<T> {
    int compare(T o1,T o2);

    default void printSomething(){
        System.out.println("haha");
    }

    static String getDemoStr(){
        return "";
    }

    /**
     * 这部分是网上的解释
     * 函数式接口里是可以包含Object里的public方法，这些方法对于函数式接口来说，
     * 不被当成是抽象方法（虽然它们是抽象方法）；因为任何一个函数式接口的实现，
     * 默认都继承了Object类，包含了来自java.lang.Object里对这些抽象方法的实现；
     *
     * 这个是马老师的解释
     * equals 作为任何一个类，都是object的子类，都已经默认实现类
     * @param o
     * @return
     */
    boolean equals(Object o);
}
