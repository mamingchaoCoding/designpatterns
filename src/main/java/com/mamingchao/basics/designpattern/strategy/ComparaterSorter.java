package com.mamingchao.basics.designpattern.strategy;

/**
 *
 * 基于选择方法排序
 *
 * Created by mamingchao on 2020/9/27.
 */
public class ComparaterSorter<T> {

    public void sort(T[] arr, MyComparator<T> comparator) {

        for (int i = 0; i < arr.length; i++) {
            int minIndex = i;

            for (int j = i+1; j < arr.length; j++) {
                minIndex = comparator.compare(arr[j],arr[minIndex]) == -1 ? j : minIndex;
            }

            swap(arr,i,minIndex);
            System.out.println(arr[i]);
        }
    }

    private void swap(T[] myComparables,int i ,int minIndex) {
        T temp = myComparables[i];
        myComparables[i] =  myComparables[minIndex];
        myComparables[minIndex] = temp;
    }
}
