package com.mamingchao.basics.designpattern.strategy;

/**
 * Created by mamingchao on 2020/9/27.
 */
public class Tester {
    public static void main(String[] args) {
        Cat[] cats = new Cat[]{new Cat(3,4),new Cat(1,2),new Cat(5,3)};
        Dog[] dogs = new Dog[]{new Dog(3),new Dog(1),new Dog(5)};

//        ComparableSorter sorter = new ComparableSorter();
//        sorter.sort(cats);
//
//
//        ComparaterSorter comparaterSorter = new ComparaterSorter();
//        comparaterSorter.sort(cats,new CatComparator2());
//
//        comparaterSorter.sort(dogs,new DogComparator());
//
//
//        comparaterSorter.sort(dogs,(o1,o2) -> {
//            //这里使用函数式变成，但是o1 o2点不出来，是因为 new ComparaterSorter 的时候，范型的类型
//            //没有指定，不指定的话默认就是Object，所以 o1 o2 点出来的都是object 的方法
////            o1.
//            return 0;
//        });

        ComparaterSorter<Cat> catComparaterSorter = new ComparaterSorter();
        catComparaterSorter.sort(cats,(o1,o2) -> {
            if (o1.getHight() > o2.getHight())
                return 1;
            else if(o1.getHight() < o2.getHight())
                return -1;
            return 0;
        });

    }
}
