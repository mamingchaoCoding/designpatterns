package com.mamingchao.basics.designpattern.strategy;

import com.mamingchao.basics.designpattern.strategy.comparable.MyComparable;

/**
 *
 * Created by mamingchao on 2020/9/27.
 */
public class Cat implements MyComparable<Cat> {

    private int weight,hight;

    public Cat(int weight, int hight) {
        this.weight = weight;
        this.hight = hight;
    }

    //按照体重升序
    @Override
    public int compareTo(Cat o) {
        if (this.getWeight() > o.getHight())
            return 1;
        else if(this.getWeight() < o.getHight())
            return -1;
        return 0;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHight() {
        return hight;
    }

    public void setHight(int hight) {
        this.hight = hight;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "weight=" + weight +
                ", hight=" + hight +
                '}';
    }
}
