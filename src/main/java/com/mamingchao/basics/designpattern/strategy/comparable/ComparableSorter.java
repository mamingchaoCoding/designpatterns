package com.mamingchao.basics.designpattern.strategy.comparable;

/**
 *
 * 基于选择排序
 *
 * Created by mamingchao on 2020/9/27.
 */
public class ComparableSorter {

    public void sort(MyComparable[] myComparables) {

        for (int i = 0; i < myComparables.length; i++) {
            int minIndex = i;

            for (int j = i+1; j < myComparables.length; j++) {
                minIndex = myComparables[j].compareTo(myComparables[minIndex]) == -1 ? j : minIndex;
            }

            swap(myComparables,i,minIndex);
            System.out.println(myComparables[i]);
        }
    }

    private void swap(MyComparable[] myComparables,int i ,int minIndex) {
        MyComparable temp = myComparables[i];
        myComparables[i] =  myComparables[minIndex];
        myComparables[minIndex] = temp;
    }
}
