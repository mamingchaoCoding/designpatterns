package com.mamingchao.basics.designpattern.strategy.comparable;

/**
 * 这里是 自己写了一个，正常 直接可以用 java.util.Comparable<T>
 * 如果不用范型，默认是Object
 * Created by mamingchao on 2020/9/27.
 */
public interface MyComparable<T> {
    //返回的是 负值，0，正值 分别代表 小于，等于，大于
    int compareTo(T o);
}
