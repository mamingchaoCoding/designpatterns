package com.mamingchao.basics.designpattern.strategy;

/**
 *
 * 按照狗身长度 从长到短 降序
 *
 * Created by mamingchao on 2020/9/27.
 */
public class DogComparator implements MyComparator<Dog>{
    @Override
    public int compare(Dog o1, Dog o2) {
        if (o1.getLength() < o2.getLength())
            return 1;
        else if(o1.getLength() > o2.getLength())
            return -1;
        return 0;
    }
}
