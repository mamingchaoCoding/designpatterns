package com.mamingchao.basics.designpattern.strategy;

import java.util.Comparator;

/**
 * 实现对象 compare 和sort 的另一种方式，就是实现 java.util.Comparator
 * Comparator 也是函数式变成
 *
 *
 * Created by mamingchao on 2020/9/29.
 */
public class ObjWithComparator implements Comparator{
    @Override
    public int compare(Object o1, Object o2) {
        return 0;
    }

    public static void main(String[] args) {
        Dog[] arr = {new Dog(1),new Dog(5),new Dog(4)};
        sort(arr,(o1,o2) -> {
            return 0;
        });
    }

    private static void sort(Object[] arr, Comparator c){

    }
}
