package com.mamingchao.basics.designpattern.strategy;

/**
 * 按照猫的身高 降序
 *
 * Created by mamingchao on 2020/9/27.
 */
public class CatComparator1 implements MyComparator<Cat>{
    @Override
    public int compare(Cat o1, Cat o2) {
        if (o1.getHight() < o2.getHight())
            return 1;
        else if(o1.getHight() > o2.getHight())
            return -1;
        return 0;
    }
}
