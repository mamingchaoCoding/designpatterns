package com.mamingchao.basics.designpattern.strategy;

import com.mamingchao.basics.designpattern.strategy.comparable.MyComparable;

/**
 * Created by mamingchao on 2020/9/27.
 */
public class Dog implements MyComparable<Dog> {

    public int length;

    public Dog(int length) {
        this.length = length;
    }


    //按照狗的身长，升序
    @Override
    public int compareTo(Dog o) {
        if (this.getLength() > o.getLength())
            return 1;
        else if(this.getLength() < o.getLength())
            return -1;
        return 0;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "length=" + length +
                '}';
    }
}
