package com.mamingchao.basics.designpattern.strategy;

/**
 * //按照体重降序
 * Created by mamingchao on 2020/9/27.
 */
public class CatComparator2 implements MyComparator<Cat>{
    @Override
    public int compare(Cat o1, Cat o2) {
        if (o1.getWeight() < o2.getWeight())
        return 1;
        else if(o1.getWeight() > o2.getWeight())
        return -1;
        return 0;
    }
}
