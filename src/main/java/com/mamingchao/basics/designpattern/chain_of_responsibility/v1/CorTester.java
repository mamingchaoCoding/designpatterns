package com.mamingchao.basics.designpattern.chain_of_responsibility.v1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mamingchao on 2020/10/13.
 */
public class CorTester {
    public static void main(String[] args) {

        Msg msg = new Msg();
        msg.setMsg("大家好，我是<script> 马明昌，I am working in viewhigh.com,大家都是996");

        //第三阶段
        FilterChain fc1 = new FilterChain();
        fc1.addFilter(new UrlFilter())
                .addFilter(new SensetiveWordFilter());

        FilterChain fc = new FilterChain();
        fc.addFilter(new ScriptFilter()).addFilter(fc1);


        fc.doFilter(msg);
        System.out.println(msg);

        //第二阶段
//        List<Filter> filters = new ArrayList();
//
//        filters.add(new ScriptFilter());
//        filters.add(new UrlFilter());
//        filters.add(new SensetiveWordFilter());
//
//        for (Filter f: filters) {
//            f.doFilter(msg);
//        }

        //第一阶段
//        ScriptFilter f1 = new ScriptFilter();
//        f1.doFilter(msg);
//        System.out.println(msg);
//
//        UrlFilter f2 = new UrlFilter();
//        f2.doFilter(msg);
//        System.out.println(msg);
//
//        SensetiveWordFilter f3 = new SensetiveWordFilter();
//        f3.doFilter(msg);
//        System.out.println(msg);

    }
}

class Msg{
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Msg{" +
                "msg='" + msg + '\'' +
                '}';
    }
}


interface Filter {
     boolean doFilter(Msg msg);
}

class ScriptFilter implements Filter{

    @Override
    public boolean doFilter(Msg msg) {
        msg.setMsg(msg.getMsg().replaceAll("<","[").replaceAll(">","]"));
        return true;
    }
}

class UrlFilter implements Filter{

    @Override
    public boolean doFilter(Msg msg) {

        if (msg.getMsg().contains("viewhigh.com")) {
            msg.setMsg(msg.getMsg().replaceAll("viewhigh.com","www.viewhigh.com"));
            return false;
        } else {
            return true;
        }
    }
}

class SensetiveWordFilter implements Filter{

    @Override
    public boolean doFilter(Msg msg) {
        if (msg.getMsg().contains("996")) {
            msg.setMsg(msg.getMsg().replaceAll("996","955"));
            return false;
        } else {
            return true;
        }
    }
}


/**
 * 实现了 Filter后，就可以实现
 * filterChain add 一个 filterChain了
 */
class FilterChain implements Filter{
    private List<Filter> filters = new ArrayList();

    //return this，可以链式调用，这个是个小技巧
    public FilterChain addFilter(Filter f){
        this.filters.add(f);
        return this;
    }

    public boolean doFilter(Msg msg){
        for (Filter f: filters) {
            boolean result = f.doFilter(msg);
            if (!result) {
                return false;
            }
        }
        return true;
    }
}

