package com.mamingchao.basics.designpattern.chain_of_responsibility.v2;

import java.util.ArrayList;
import java.util.List;

/**
 * 我测了一下，链A 拼接 链B 有问题，就一个链条是没问题的
 *
 * 模拟 servlet Filter chain 的责任链实现
 * request 处理顺序是 Filter1，Filter2，Filter3
 * response 处理顺序是 Filter3，Filter2，Filter1
 *
 * Created by mamingchao on 2020/10/13.
 */
public class ServletTester {
    public static void main(String[] args) {

        MyRequest req = new MyRequest();
        req.str = "大家好，我是<script> 马明昌，I am working in viewhigh.com,大家都是996";

        MyResponse res = new MyResponse();
        res.str = "";

        ServeltFilterChain fc1 = new ServeltFilterChain();
        fc1.addFilter(new ServletUrlFilter())
                .addFilter(new ServletSensitiveWordFilter());

        ServeltFilterChain fc = new ServeltFilterChain();
        fc.addFilter(new ServletScriptFilter()).addFilter(fc1);


        fc.doFilter(req,res,fc);
        System.out.println(req);
        System.out.println(res);


    }
}
    interface ServletFilter {
        void doFilter(MyRequest req, MyResponse res, ServeltFilterChain chain);
    }

    class MyRequest {
        String str;

        @Override
        public String toString() {
            return "MyRequest{" +
                    "str='" + str + '\'' +
                    '}';
        }
    }

    class MyResponse {
        String str;

        @Override
        public String toString() {
            return "MyResponse{" +
                    "str='" + str + '\'' +
                    '}';
        }
    }

    class ServletScriptFilter implements ServletFilter {

        @Override
        public void doFilter(MyRequest req, MyResponse res, ServeltFilterChain chain) {
            req.str = req.str.replaceAll("<", "[").replaceAll(">", "]");
            chain.doFilter(req,res,chain);
            res.str += " response script filter";
        }
    }

    class ServletUrlFilter implements ServletFilter {

        @Override
        public void doFilter(MyRequest req, MyResponse res, ServeltFilterChain chain) {
            req.str = req.str.replaceAll("viewhigh.com", "www.viewhigh.com");
            chain.doFilter(req,res,chain);
            res.str += " response Url filter";
        }
    }

    class ServletSensitiveWordFilter implements ServletFilter {
        @Override
        public void doFilter(MyRequest req, MyResponse res, ServeltFilterChain chain) {
            req.str = req.str.replaceAll("996", "955");
            chain.doFilter(req,res,chain);
            res.str += " response sensitive word filter";
        }
    }


    /**
     * 实现了 Filter后，就可以实现
     * filterChain add 一个 filterChain了
     */
    class ServeltFilterChain implements ServletFilter {
        int index = 0;
        private List<ServletFilter> filters = new ArrayList();

        //return this，可以链式调用，这个是个小技巧
        public ServeltFilterChain addFilter(ServletFilter f) {
            this.filters.add(f);
            return this;
        }

        public void doFilter(MyRequest req, MyResponse res, ServeltFilterChain chain) {

            if (index == filters.size())
                return;
            ServletFilter f = filters.get(index);
            index ++;
            f.doFilter(req, res,chain);
        }
    }
