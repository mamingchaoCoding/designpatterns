package com.mamingchao.basics.designpattern.bridge;

/**
 * Created by mamingchao on 2020/10/20.
 */
public class Main {

    public static void main(String[] args) {

    }
}

class GG{
    void chase(MM mm) {
        Gift g = new ExpensiveGift(new Book());
        this.give(mm,g);
    }


    void give(MM m, Gift g){

    }
}

class MM{}
