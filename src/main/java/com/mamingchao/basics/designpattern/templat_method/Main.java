package com.mamingchao.basics.designpattern.templat_method;

/**
 * 模版模式
 * 就是回调
 *
 * Created by mamingchao on 2020/10/22.
 */
public class Main {
    public static void main(String[] args) {
        Breakfast mom = new MomsBreakfast();
        mom.mealCombo1();

        Breakfast dad = new DadsBreakfast();
        dad.mealCombo3();
    }
}

abstract class Breakfast{
    void mealCombo1(){
        friedEggs();
        warmMilk();
    }

    void mealCombo2(){
        friedSausage();
        warmMilk();
    }

    void mealCombo3(){
        friedEggs();
        bread();
    }

    protected abstract void friedEggs();
    protected abstract void warmMilk();
    protected abstract void friedSausage();
    protected abstract void bread();
}

class MomsBreakfast extends Breakfast{

    @Override
    protected void friedEggs() {
        System.out.println("mom eat fried eggs as a breakfast");
    }

    @Override
    protected void warmMilk() {
        System.out.println("mom drink warm milk as a breakfast");
    }

    @Override
    protected void friedSausage() {
        System.out.println("mom eat fried sausage as a breakfast");
    }

    @Override
    protected void bread() {
        System.out.println("mom eat bread as a breakfast");
    }
}

class DadsBreakfast extends Breakfast{

    @Override
    protected void friedEggs() {
        System.out.println("dad eat fried eggs as a breakfast");
    }

    @Override
    protected void warmMilk() {
        System.out.println("dad drink warm milk as a breakfast");
    }

    @Override
    protected void friedSausage() {
        System.out.println("dad eat fried sausage as a breakfast");
    }

    @Override
    protected void bread() {
        System.out.println("dad eat bread as a breakfast");
    }
}

