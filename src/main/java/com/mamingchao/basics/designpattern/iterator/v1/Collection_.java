package com.mamingchao.basics.designpattern.iterator.v1;

/**
 * Created by mamingchao on 2020/10/19.
 */
public interface Collection_ {

    void add(Object o);

    int size();

    void remove(Object o);

    Object get (int index);

    Iterator_ iterator();
}
