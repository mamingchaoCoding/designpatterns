package com.mamingchao.basics.designpattern.iterator.v1;

/**
 * Created by mamingchao on 2020/10/19.
 */
public class LinkedList_ implements Collection_{
    Node head;
    Node tail;

    int index;

    @Override
    public void add(Object o) {
        Node node = new Node(o,null);
        if (index == 0) {
            head = node;
            tail = node;
        } else {
            tail.next = node;
            tail = node;
        }
        index ++;
    }

    @Override
    public int size() {
        return index;
    }

    @Override
    public void remove(Object o) {
        //TODO
    }

    @Override
    public Object get(int index) {
        return null;
    }

    @Override
    public Iterator_ iterator() {
        return new LinkedListIterator_();
    }

    class LinkedListIterator_ implements Iterator_{

        Node currentNode = head;

        @Override
        public boolean hasNext() {
            if (currentNode.next != null)
                return true;
            return false;
        }

        @Override
        public Object next() {
            Node result =  currentNode.next;
            currentNode = result;
            return result;
        }
    }


    class Node{
        Object content;
        Node next;

        public Node(Object content, Node next) {
            this.content = content;
            this.next = next;
        }
    }
}


