package com.mamingchao.basics.designpattern.iterator.v2;

/**
 * Created by mamingchao on 2020/10/19.
 */
public interface Iterator_<E> {
    boolean hasNext();

    E next();
}
