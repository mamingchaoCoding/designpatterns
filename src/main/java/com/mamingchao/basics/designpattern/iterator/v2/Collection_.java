package com.mamingchao.basics.designpattern.iterator.v2;

/**
 * Created by mamingchao on 2020/10/19.
 */
public interface Collection_<E> {

    void add(E o);

    int size();

    void remove(E o);

    E get(int index);

    Iterator_ iterator();
}
