package com.mamingchao.basics.designpattern.proxy.spring_aop.xmlConfig;

/**
 * Created by mamingchao on 2020/10/19.
 */
public class LogProxy {
    private void before(){System.out.println("Start  logging....");}
    private void after(){System.out.println("End  logging....");}
}
