package com.mamingchao.basics.designpattern.proxy.spring_aop.xmlConfig;

import com.mamingchao.basics.designpattern.proxy.spring_aop.Tank;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by mamingchao on 2020/10/19.
 */
public class Main {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext cxt = new ClassPathXmlApplicationContext("spring_auto_aop.xml");

        Tank tank = (Tank)cxt.getBean("tank");
        tank.move();
    }
}
