package com.mamingchao.basics.designpattern.proxy.spring_aop.autoConfig;

import com.mamingchao.basics.designpattern.proxy.spring_aop.Tank;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by mamingchao on 2020/10/19.
 */
public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext cxt =  new AnnotationConfigApplicationContext(AppConfig.class);

        Tank tank = (Tank)cxt.getBean("tank");
        tank.move();
    }
}
