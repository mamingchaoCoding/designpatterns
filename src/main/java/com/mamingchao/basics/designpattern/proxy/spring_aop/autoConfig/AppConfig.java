package com.mamingchao.basics.designpattern.proxy.spring_aop.autoConfig;

import com.mamingchao.basics.designpattern.proxy.spring_aop.Tank;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Created by mamingchao on 2020/12/13.
 */
@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AppConfig {

    @Bean
    public Tank tank(){
        return new Tank();
    }
}
