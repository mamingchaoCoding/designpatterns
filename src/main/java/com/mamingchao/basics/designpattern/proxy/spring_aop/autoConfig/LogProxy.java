package com.mamingchao.basics.designpattern.proxy.spring_aop.autoConfig;


import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 * Created by mamingchao on 2020/10/19.
 */
@Aspect
public class LogProxy {
    @Before("execution(void com.mamingchao.basics.designpattern.proxy.spring_aop.Tank.move())")
    private void before(){System.out.println("Start  logging....");}
    @After("execution(void com.mamingchao.basics.designpattern.proxy.spring_aop.Tank.move())")
    private void after(){System.out.println("End  logging....");}
}
