package com.mamingchao.basics.designpattern.command;

/**
 * Created by mamingchao on 2020/10/21.
 */
public class Context {
    String msg = "This is a common command!";
}
