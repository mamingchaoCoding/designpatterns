package com.mamingchao.basics.designpattern.command;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mamingchao on 2020/10/21.
 */
public class CommandChain implements Command{

    List<Command> commands = new ArrayList();

    CommandChain add(Command command) {
        this.commands.add(command);
        return this;
    }

    @Override
    public void exec() {
        for (int i = 0; i < commands.size(); i++) {
            commands.get(i).exec();
        }
    }

    @Override
    public void undo() {
        for (int i = 0; i < commands.size(); i++) {
            commands.get(i).undo();
        }
    }
}
