package com.mamingchao.basics.designpattern.flyweight.with_composite;

import java.util.List;

/**
 * Created by mamingchao on 2020/10/23.
 */
public abstract class ObjectNode {
    boolean isUsing = false;
    String name;
    abstract void print(int depth);
}

class AtomicNode extends ObjectNode {

    public AtomicNode(String nodeName) {
        this.name = nodeName;
    }

    @Override
    void print(int depth) {
        for (int i = 0; i < depth; i++) {
            System.out.print( "  ");
        }
        System.out.println( name);
    }
}

class CompositeNode extends ObjectNode{

    List<ObjectNode> nodes;

    public CompositeNode(String nodeName, List<ObjectNode> nodes) {
        this.name = nodeName;
        this.nodes = nodes;
    }
    void addNode(ObjectNode node){
        this.nodes.add(node);
    }

    @Override
    void print(int depth) {
        String space = "";
        for (int i = 0; i < depth; i++) {
            space +="  ";
        }
        System.out.println(name);
        System.out.println("{");
        tree(this,1);
        System.out.println("}");
    }

    private void tree(CompositeNode compositeNode, int depth) {
        for(ObjectNode node : compositeNode.nodes){
            node.print(depth);
            if (node instanceof CompositeNode) {
                tree((CompositeNode)node,depth+1);
            }
        }
    }
}


