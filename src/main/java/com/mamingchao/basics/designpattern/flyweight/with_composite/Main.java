package com.mamingchao.basics.designpattern.flyweight.with_composite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mamingchao on 2020/10/23.
 */
public class Main {
    public static void main(String[] args) {
        ObjectNodePool pool = new ObjectNodePool();
        for (int i = 0; i <1 ; i++) {
            pool.add(new AtomicNode("Atomic-"+i));
        }

        for (int i = 0; i <10 ; i++) {
            List<ObjectNode> lists = new ArrayList<>();
            lists.add(pool.poll());
            lists.add(pool.poll());
            pool.add(new CompositeNode("Composite-"+i,lists));
        }

        for (int i = 0; i <pool.size() ; i++) {
            pool.poll().print(0);
        }
    }
}
