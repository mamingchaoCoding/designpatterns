package com.mamingchao.basics.designpattern.state.homework;

/**
 * Created by mamingchao on 2020/10/23.
 */
public class Car {

    CarAction carAction;

    public Car(CarAction carAction) {
        this.carAction = carAction;
    }

    void run(){
        carAction.runTheCar();
    }

    void stop() {
        carAction.stopTheCar();
    }

    void openDoor() {
        carAction.openTheDoor();
    }

    void closeDoor() {
        carAction.closeTheDoor();
    }
}
