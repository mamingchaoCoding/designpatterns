package com.mamingchao.basics.designpattern.state.v2;

/**
 * Created by mamingchao on 2020/10/22.
 */
public abstract class MMState {

    abstract void smile(String name);
    abstract void cry(String name);
    abstract void say(String name);
}
