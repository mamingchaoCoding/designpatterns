package com.mamingchao.basics.designpattern.state.v2;

/**
 * Created by mamingchao on 2020/10/22.
 */
public class SadState extends MMState {
    @Override
    void smile(String name) {
        System.out.println(name + ",pretty girl are smiling sadly");
    }

    @Override
    void cry(String name) {
        System.out.println(name + ",pretty girl are crying sadly");
    }

    @Override
    void say(String name) {
        System.out.println(name + ",pretty girl are saying sadly");
    }
}
