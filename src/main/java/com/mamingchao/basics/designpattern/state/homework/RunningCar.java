package com.mamingchao.basics.designpattern.state.homework;

/**
 * Created by mamingchao on 2020/10/23.
 */
public class RunningCar implements CarAction {
    @Override
    public void openTheDoor() {
        System.out.println("Ooh, you can't do that! Your car is running, you can't open its door!");
    }

    @Override
    public void closeTheDoor() {
        System.out.println("Ooh, you can't do that! Your car is running, you can't close its door!");
    }

    @Override
    public void runTheCar() {
        System.out.println("Ooh, you can't do that! Your car is already running, you can't run it again!");
    }

    @Override
    public void stopTheCar() {
        System.out.println("Ok, the car is stopped");
    }
}
