package com.mamingchao.basics.designpattern.state.v2;

/**
 * Created by mamingchao on 2020/10/22.
 */
public class HappyState extends MMState {
    @Override
    void smile(String name) {
        System.out.printf("pretty girl %s are smiling happily",name);
    }

    @Override
    void cry(String name) {
        System.out.printf("pretty girl %s are crying happily",name);
    }

    @Override
    void say(String name) {
        System.out.printf("pretty girl %s are saying happily",name);
    }
}
