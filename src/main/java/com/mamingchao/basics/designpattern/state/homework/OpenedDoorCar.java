package com.mamingchao.basics.designpattern.state.homework;

/**
 * Created by mamingchao on 2020/10/23.
 */
public class OpenedDoorCar implements CarAction {
    @Override
    public void openTheDoor() {
        System.out.println("Ooh, you can't do that! You have already opened the car door!");
    }

    @Override
    public void closeTheDoor() {
        System.out.println("The car door is closed!");
    }

    @Override
    public void runTheCar() {
        System.out.println("Ooh, you can't do that! Your car's door is opening, you can't run it!");
    }

    @Override
    public void stopTheCar() {
        System.out.println("Ooh, you can't do that! Your car's door is opening, you can't stop it!");
    }
}
