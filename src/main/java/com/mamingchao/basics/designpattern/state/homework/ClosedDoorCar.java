package com.mamingchao.basics.designpattern.state.homework;

/**
 * Created by mamingchao on 2020/10/23.
 */
public class ClosedDoorCar implements CarAction {

    @Override
    public void openTheDoor() {
        System.out.println("The car door is opened!");
    }

    @Override
    public void closeTheDoor() {
        System.out.println("Ooh, you can't do that! You have already closed the car door!");

    }

    @Override
    public void runTheCar() {
        System.out.println("Yeah hoo, the car is running!");
    }

    @Override
    public void stopTheCar() {
        System.out.println("The car is stopped !");
    }
}
