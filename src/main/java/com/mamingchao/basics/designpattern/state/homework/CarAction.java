package com.mamingchao.basics.designpattern.state.homework;

/**
 * Created by mamingchao on 2020/10/23.
 */
public interface CarAction {
    void openTheDoor();
    void closeTheDoor();
    void runTheCar();
    void stopTheCar();
}
