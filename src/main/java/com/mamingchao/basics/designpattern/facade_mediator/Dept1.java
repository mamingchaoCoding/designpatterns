package com.mamingchao.basics.designpattern.facade_mediator;

/**
 * 民政局
 * Created by mamingchao on 2020/10/13.
 */
public class Dept1 {
    public void dealWithMarriage(String boyName,String girlName){
        System.out.printf("Congratulations,%s and %s" , boyName, girlName);
        System.out.println();
    }
}
