package com.mamingchao.basics.designpattern.facade_mediator;

/**
 * Created by mamingchao on 2020/10/13.
 */
public class Facade {

    Dept1 d1 = new Dept1();
    Dept2 d2 = new Dept2();
    Dept3 d3 = new Dept3();

    public void reception(String boyName,String girlName){
        d1.dealWithMarriage(boyName, girlName);
        d2.dealWithHouseProperty();
        d3.prove();
    }
}
