package com.mamingchao.basics.designpattern.builder;

/**
 * Created by mamingchao on 2020/10/20.
 */
public class Main {

    public static void main(String[] args) {
        Person p = new Person.PersonBuilder()
                .buildBasic("mamingchao",true)
                .buildHeight(167.5)
//                .buildWeight(72.5)
                .buildHomeTown("黑龙江","黑河","七队")
                .buildLocation("","").build();

        System.out.println(p.toString());

        ComplexTerrainBuilder builder = new ComplexTerrainBuilder();
        Terrain t = builder.buildFort().buildMine().buildWall().build();
        System.out.println(t.toString());
    }
}
