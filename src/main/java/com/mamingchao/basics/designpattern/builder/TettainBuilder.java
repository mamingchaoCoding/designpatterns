package com.mamingchao.basics.designpattern.builder;

/**
 * Created by mamingchao on 2020/10/20.
 */
public interface TettainBuilder {
    TettainBuilder buildWall();
    TettainBuilder buildFort();
    TettainBuilder buildMine();

    Terrain build();
}
