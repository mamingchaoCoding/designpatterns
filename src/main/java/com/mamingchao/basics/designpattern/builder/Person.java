package com.mamingchao.basics.designpattern.builder;

/**
 * 如果一个类初始化的参数很多
 * 并且 这个类的的 attribute field 可以 分组构造
 *
 * 可以参考下面的写法
 *
 * Created by mamingchao on 2020/10/20.
 */
public class Person {
    String name;
    boolean gender;
    double height;
    double weight;

    HomeTown homeTown;

    Location loc;

    Person(){}


    public static class PersonBuilder{
        Person p = new Person();

        public PersonBuilder buildBasic(String name,boolean gender){
            p.name = name;
            p.gender = gender;
            return this;
        }

        public PersonBuilder buildHeight(double height) {
            p.height = height;
            return this;
        }

        public PersonBuilder buildWeight(double weight) {
            p.weight = weight;
            return this;
        }

        public PersonBuilder buildHomeTown(String province,String city,String country){
            p.homeTown = new HomeTown(province, city, country);
            return this;
        }

        public PersonBuilder buildLocation(String street, String roomNo){
            p.loc = new Location(street, roomNo);
            return this;
        }

        public Person build(){
            return p;
        }
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", gender=" + gender +
                ", height=" + height +
                ", weight=" + weight +
                ", homeTown=" + homeTown +
                ", loc=" + loc +
                '}';
    }
}

class HomeTown{
    String province;
    String city;
    String country;

    public HomeTown(String province, String city, String country) {
        this.province = province;
        this.city = city;
        this.country = country;
    }

    @Override
    public String toString() {
        return "HomeTown{" +
                "province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}

class Location{
    String street;
    String roomNo;

    public Location(String street, String roomNo) {
        this.street = street;
        this.roomNo = roomNo;
    }

    @Override
    public String toString() {
        return "Location{" +
                "street='" + street + '\'' +
                ", roomNo='" + roomNo + '\'' +
                '}';
    }
}
