package com.mamingchao.basics.designpattern.builder;

/**
 *
 * 坦克游戏 里的地形
 * Created by mamingchao on 2020/10/20.
 */
public class Terrain {
    Wall wall;
    Fort fort;
    Mine mine;

    @Override
    public String toString() {
        return "Terrain{" +
                "wall=" + wall +
                ", fort=" + fort +
                ", mine=" + mine +
                '}';
    }
}

//地形中的墙
class Wall{
    int x,y,w,h;

    public Wall(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    @Override
    public String toString() {
        return "Wall{" +
                "x=" + x +
                ", y=" + y +
                ", w=" + w +
                ", h=" + h +
                '}';
    }
}

//地形中的碉堡
class Fort{
    int x,y,w,h;

    public Fort(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }
    @Override
    public String toString() {
        return "Wall{" +
                "x=" + x +
                ", y=" + y +
                ", w=" + w +
                ", h=" + h +
                '}';
    }
}

//地雷
class Mine{
    int x,y,w,h;

    public Mine(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    @Override
    public String toString() {
        return "Wall{" +
                "x=" + x +
                ", y=" + y +
                ", w=" + w +
                ", h=" + h +
                '}';
    }
}
