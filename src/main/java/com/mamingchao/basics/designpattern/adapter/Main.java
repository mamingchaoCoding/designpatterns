package com.mamingchao.basics.designpattern.adapter;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * java.io 里是 Adapter的一种实现
 *
 * Created by mamingchao on 2020/10/20.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("/Users/mamingchao/work/viewhigh/designmodel/src/images/test.txt");

        InputStreamReader isr = new InputStreamReader(fis);

        BufferedReader br = new BufferedReader(isr);

        StringBuffer sb = new StringBuffer();

        sb.append(br.readLine());

        System.out.println(sb.toString());
        br.close();
        isr.close();
        fis.close();

        /**
         * 通过 下面 awt 里的 windows Listener
         *
         * 这个Windows Adaper 虽然叫Adapter，但是不是Adapter 设计模式的一种事件
         * 因为添加WindowsListener的时候，要实现好多方法，而你可能并不是想要实现所有
         *
         * 这里的WindowsAdapter 是实现里WindowsListener的 abstract 类
         * 这样实现，f.addWindowListener(new WindowAdapter(){@code })
         * 需要实现哪个，就写哪个就行。
         */
        Frame f = new Frame();
        f.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {

            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });

        f.addWindowListener(new WindowAdapter() {
            /**
             * Invoked when a window has been opened.
             *
             * @param e
             */
            @Override
            public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
            }
        });

    }
}
