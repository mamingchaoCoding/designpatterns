package com.mamingchao.basics.designpattern.singleton;

/**
 * 静态内部类
 * 这是其中一个完美的单例写法
 * jvm保证 类加载 只加载一次
 *
 * Created by mamingchao on 2020/10/9.
 */
public class LazyMan6 {

    private LazyMan6(){}

    private static class LazyMan6Holder{
        private static final LazyMan6 instance = new LazyMan6();
    }

    public static LazyMan6 getInstance(){
        return LazyMan6Holder.instance;
    }


    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
//                System.out.println(LazyMan.getInstance());
                System.out.println(LazyMan6.getInstance().hashCode());
            }).start();
        }
    }
}
