package com.mamingchao.basics.designpattern.singleton;

/**
 * 妄图通过减少同步代码块优化提高效率，然而还是线程不安全
 *
 * Created by mamingchao on 2020/10/9.
 */
public class LazyManWithLockErrorEvolution4 {
    private static LazyManWithLockErrorEvolution4 instance;

    private LazyManWithLockErrorEvolution4(){}

    public static LazyManWithLockErrorEvolution4 getInstance(){
        if (instance == null) {
            //妄图通过减少同步代码块优化提高效率，然而还是线程不安全
            synchronized(LazyManWithLockErrorEvolution4.class) {
                instance = new LazyManWithLockErrorEvolution4();
            }
        }
        return instance;
    }


    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
//                System.out.println(LazyMan.getInstance());
                System.out.println(LazyManWithLockErrorEvolution4.getInstance().hashCode());
            }).start();
        }
    }
}
