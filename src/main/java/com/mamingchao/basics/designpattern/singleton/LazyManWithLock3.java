package com.mamingchao.basics.designpattern.singleton;

import java.util.concurrent.TimeUnit;

/**
 * 线程安全了，但是效率低
 *
 * Created by mamingchao on 2020/10/9.
 */
public class LazyManWithLock3 {
    private static LazyManWithLock3 instance;

    private LazyManWithLock3(){}

    public static synchronized LazyManWithLock3 getInstance(){
        if (instance == null) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            instance = new LazyManWithLock3();
        }
        return instance;
    }


    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
//                System.out.println(LazyMan.getInstance());
                System.out.println(LazyManWithLock3.getInstance().hashCode());
            }).start();
        }
    }
}
