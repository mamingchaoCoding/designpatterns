package com.mamingchao.basics.designpattern.singleton;

/**
 * Created by mamingchao on 2020/10/9.
 * 构造方法 private
 * jvm保证线程安全，因为每个class只能load到内存里一次
 * 简单实用，推荐
 * Class.forName("") 把类加载到内存里，但是并不实例化
 * 但是static的，加载到内存就实例化
 *
 * 静态语句块
 *
 * 同一个类的不同对象的hashcode 是不同的
 * hashcode相同也可能是不是同一个对象，但是 同一个类的不同对象的hashcode 是不同的
 */
public class HungeryMan1 {
    private static HungeryMan1 instance;

    static {
        instance = new  HungeryMan1();
    }

    private HungeryMan1(){}

    public static HungeryMan1 getInstance(){
        return instance;
    }


    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
//                System.out.println(HungeryMan.getInstance());
                System.out.println(HungeryMan1.getInstance().hashCode());
            }).start();
        }
    }
}
