package com.mamingchao.basics.designpattern.singleton;

import java.util.concurrent.TimeUnit;

/**
 * 线程不安全
 *
 * Created by mamingchao on 2020/10/9.
 */
public class LazyMan2 {
    private static LazyMan2 instance;

    private LazyMan2(){}

    public static LazyMan2 getInstance(){
        if (instance == null) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            instance = new LazyMan2();
        }
        return instance;
    }


    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
//                System.out.println(LazyMan.getInstance());
                System.out.println(LazyMan2.getInstance().hashCode());
            }).start();
        }
    }
}
